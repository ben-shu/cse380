<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="tileset" tilewidth="128" tileheight="128" tilecount="2" columns="2">
 <image source="tileset.png" width="256" height="128"/>
 <tile id="0">
  <properties>
   <property name="collide" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
