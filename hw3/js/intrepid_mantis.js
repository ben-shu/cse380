/* Create Phaser configuration settings. */
var config = {
    type: Phaser.AUTO,
    width: 1536,
    height: 1536,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var player;
var cursors;

var map;
var tiles;
var layerBG, layerWalls;

var camera;

const tileWidth = 128;
const tileHeight = 128;

var start;
var end;

const tileObj = {
    x:  -1,
    y:  -1,
    total: 0,
    steps:  0,
    remain:  0,
    prev: null
}

var openList, closedList;

var mantisX = 0;
var mantisY = 0;

var velocity = 128;
const minSpeed = 128;
const maxSpeed = 640;

/* Create new game object. */
var game = new Phaser.Game(config);

function preload() {
    /* Load main spritesheet for player's mantis. */
    this.load.spritesheet('mantis', 'assets/images/praying_mantis.png',
                { frameWidth: 128, frameHeight: 128 });
    
    /* Load tileset image and map information. */
    this.load.image('tiles', 'maps/tileset.png');
    this.load.tilemapTiledJSON('map', 'maps/map.json');
};

function create() {
    /* Create new tilemap object. */
    map = this.make.tilemap({ key: 'map' });

    /* Apply tileset.png to the map. */
    this.tiles = map.addTilesetImage('tileset', 'tiles');

    /* Create layers for tiled map. */
    layerBG = map.createDynamicLayer('Background', this.tiles, 0, 0);
    layerWalls = map.createDynamicLayer('Walls', this.tiles, 0, 0);
    layerWalls.setCollisionByProperty({ collide: true });
    
    /* Add mantis sprite for player to use. */
    player = this.physics.add.sprite(64, 64, 'mantis');
    player.setCollideWorldBounds(true);

    /* Detect collisions between player and walls. */
    this.physics.add.collider(player, layerWalls);

    /* Add animations to mantis sprite. */
    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('mantis', { start: 2, end: 3 }),
        frameRate: 2,
        repeat: -1
    });

    this.anims.create({
        key: 'idle',
        frames: [ { key: 'mantis', frame: 0 }, { key: 'mantis', frame: 3 } ],
        frameRate: 2,
        repeat: 1
    });

    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('mantis', { start: 0, end: 1 }),
        frameRate: 2,
        repeat: -1
    });
    
    /* Set up to detect keyboard input. */
    this.cursors = this.input.keyboard.addKeys({
        up:     Phaser.Input.Keyboard.KeyCodes.W,
        down:   Phaser.Input.Keyboard.KeyCodes.S,
        left:   Phaser.Input.Keyboard.KeyCodes.A,
        right:  Phaser.Input.Keyboard.KeyCodes.D,
        M:      Phaser.Input.Keyboard.KeyCodes.M,
        N:      Phaser.Input.Keyboard.KeyCodes.N,
        L:      Phaser.Input.Keyboard.KeyCodes.L
    });

    this.camera = this.cameras.main;
    this.camera.setBounds(0, 0, 1920, 1920, true);
    this.camera.height = 768;
    this.camera.width = 768;
    this.camera.scrollX = 0;
    this.camera.scrollY = 0;
    this.camera.startFollow(player);
};

var mPress = false, nPress = false, mousePress = false;

function update(){
    if (this.cursors.left.isDown) {
        player.setVelocityX(velocity * -1);
        player.anims.play('left', true);
    } else if (this.cursors.right.isDown) {
        player.setVelocityX(velocity * 1);
        player.anims.play('right', true);
    } else { player.setVelocityX(0); }

    if (this.cursors.up.isDown){
        player.setVelocityY(velocity * -1);
    } else if (this.cursors.down.isDown) {
        player.setVelocityY(velocity * 1);
    } else {
        player.setVelocityY(0);
    }

    if (this.cursors.M.isUp) { mPress = false; }
    if (this.cursors.N.isUp) { nPress = false; }

    if (this.cursors.M.isDown) {
        if (!mPress) {
            mPress = true;
            if (velocity < maxSpeed) {
                velocity += 128;
            }
        }
    } else if (this.cursors.N.isDown) {
        if (!nPress) {
            nPress = true;
            if (velocity > minSpeed) {
                velocity -= 128;
            }
        }
    }

    if (player.velocityX == 0 && player.velocityY == 0) { player.anims.play('idle'); }

    if (this.game.input.activePointer.isDown) {
        if (!mousePress) {
            mousePress = true;
            var x = this.game.input.activePointer.x, y = this.game.input.activePointer.y;
            console.log("Click!  x: " + x + ", y: " + y);
            x = (Math.floor(x / 128) * 128) + 64;
            y = (Math.floor(y / 128) * 128) + 64;
            console.log("Tile at x: " + x + ", y: " + y);

            var tile = map.getTileAt(Math.floor(x / 128), Math.floor(y / 128), false, layerWalls);
            if (tile != null) {
                return;
            } else {
                this.end = {
                    x: x,
                    y: y,
                    final: 0,
                    steps: 0,
                    remain: 0,
                    prev: null
                };
                
                this.start = {
                    x: (Math.floor(player.x / 128) * 128) + 64,
                    y: (Math.floor(player.y / 128) * 128) + 64,
                    final: 0,
                    steps: 0,
                    remain: 0,
                    prev: null
                };
                console.log(this.start);
                console.log(this.end);
                var path = findPath(this.start, this.end, map);
                console.log(path);
            }
        }
    } else { mousePress = false; }
};

var moving = false;

function findPath(start, end, map) {
    /* Declare empty array for neighbors of closed list tiles. */
    var adjacent, path;
    var current = start;
    var next;

    openList = new Array();
    closedList = new Array();

    closedList.push(start);

    /* Iterate through all possible paths until end is reached. */
    while (current.x != end.x || current.y != end.y) {
        console.log("Finding adjacent tiles...");
        /* Check neighbors of current tile. */
        adjacent = this.getAdjacent(current, end, map);
        /* If tile has neighbors, find  */
        if (adjacent.length > 0){
            var i = 0;
            for (i = 0; i < adjacent.length; i++) {
                if (!closedList.includes(adjacent[i])) {
                    if (!openList.includes(adjacent[i])) { openList.push(adjacent[i]); }
                }
            }
            /* Retrieve cheapest possible tile. */
            next = this.getCheapest(openList);
            next.prev = current;
            /* Remove chosen tile from open list and add to closed list. */
            openList.splice(openList.indexOf(next), 1);
            closedList.push(next);
            current = next;
        } else {
            console.log("Dead end!");
            openList.splice(openList.indexOf(current), 1);
            closedList.push(current);
            if (current.prev == null) {
                return null;
            } else {
                current = current.prev;
            }
        }
    }
    path = new Array();
    while (current.prev != null) {
        path.unshift(current);
        current = current.prev;
    }
    return path;
};

function getCheapest(list) {
    var i = 0, min = list[0];
    for (i = 1; i < list.length; i++) {
        if (list[i].total < min.total) { min = list[i]; }
    }
    return min;
}

function getAdjacent(current, end, map) {
    /* Create empty tile objects for checking neighbors. */
    console.log(map);
    console.log(this.layerBG);
    console.log(this.layerWalls);
    var adjacent = new Array();
    var up = { x: 0, y: 0, final: 0, steps: 0, remain: 0, prev: null },
        down = { x: 0, y: 0, final: 0, steps: 0, remain: 0, prev: null },
        left = { x: 0, y: 0, final: 0, steps: 0, remain: 0, prev: null },
        right = { x: 0, y: 0, final: 0, steps: 0, remain: 0, prev: null };
    up.x = current.x;
    up.y = current.y - 128;
    console.log(up);
    console.log(map.getTileAt(up.x, up.y, true, this.layerWalls));
    console.log(map.getTileAt(up.x, up.y, false, this.layerBG));
    if (map.getTileAt(up.x, up.y, true, this.layerWalls) == null
    && map.getTileAt(up.x, up.y, false, this.layerBG) != null) {
        console.log("Adding up...");
        up.steps = current.steps + 1;
        up.remain = Math.floor(Math.abs(end.x - up.x) / 128) + Math.floor(Math.abs(end.y - up.y));
        up.final = up.steps + up.remain;
        adjacent.push(up);
    }

    down.x = current.x;
    down.y = current.y + 128;
    console.log(down);
    console.log(map.getTileAt(down.x, down.y, true, this.layerWalls));
    console.log(map.getTileAt(down.x, down.y, false, this.layerBG));
    if (map.getTileAt(down.x, down.y, true, layerWalls) == null
    && map.getTileAt(down.x, down.y, false, layerBG) != null) {
        console.log("Adding down...");
        down.steps = current.steps + 1;
        down.remain = Math.floor(Math.abs(end.x - down.x) / 128) + Math.floor(Math.abs(end.y - down.y));
        down.final = down.steps + down.remain;
        adjacent.push(down);
    }

    left.x = current.x - 128;
    left.y = current.y;
    console.log(left);
    if (map.getTileAt(left.x, left.y, true, layerWalls) == null
    && map.getTileAt(left.x, left.y, false, layerBG) != null) {
        console.log("Adding left...");
        left.steps = current.steps + 1;
        left.remain = Math.floor(Math.abs(end.x - left.x) / 128) + Math.floor(Math.abs(end.y - left.y));
        left.final = left.steps + left.remain;
        adjacent.push(left);
    }

    right.x = current.x + 128;
    right.y = current.y;
    console.log(right);
    if (map.getTileAt(right.x, right.y, true, layerWalls) == null
    && map.getTileAt(right.x, right.y, false, layerBG) != null) {
        console.log("Adding right...");
        right.steps = current.steps + 1;
        right.remain = Math.floor(Math.abs(end.x - right.x) / 128) + Math.floor(Math.abs(end.y - right.y));
        right.final = right.steps + right.remain;
        adjacent.push(right);
    }

    return adjacent;
};