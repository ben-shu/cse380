'use strict'

class UIComponent {
    constructor() {}

    init(canvasId, textId, scene) {
        var canvas = document.getElementById(canvasId);
        this.spriteToDrag = null;
        this.dragOffsetX = -1;
        this.dragOffsetY = -1;

        canvas.onmousedown = function(event) {
            var mousePressX = event.clientX;
            var mousePressY = event.clientY;

            var sprite = scene.getFirstSpriteAt(mousePressX, mousePressY);
            if (sprite != null) {
                // START DRAGGING IT
                this.spriteToDrag = sprite;

                this.dragOffsetX = sprite.position.getX() - mousePressX;
                this.dragOffsetY = sprite.position.getY() - mousePressY;
            }
        }
        canvas.onmousemove = function(event) {
            window.wolfie.ui.currentMouseX = event.clientX;
            window.wolfie.ui.currentMouseY = event.clientY;
            if (this.spriteToDrag != null) {
                this.spriteToDrag.position.set(event.clientX + this.dragOffsetX, event.clientY + this.dragOffsetY, this.spriteToDrag.position.getZ(), this.spriteToDrag.position.getW());
                window.wolfie.graphics.renderScene(window.wolfie.scene);
            }
        }
        canvas.onmouseup = function(event) {
            this.spriteToDrag = null;
        }
        canvas.onmouseleave = function(event) {
            this.spriteToDrag = null;
        }

        /* Add double-click function to enable cycling through sprites on command. */
        canvas.ondblclick = function(e) {
            var clickX = e.clientX, clickY = e.clientY;
            var sprite = scene.getFirstSpriteAt(clickX, clickY);
            if (sprite != null) {
                /* Make sprite move on to the next animation state. */
                sprite.nextAnimation();
            }
        }

        /* Add eventListener to toggle text display on pressing "t". */
        var textCanvas = document.getElementById(textId);
        document.addEventListener("keydown", function(e) {
            var code = e.key;
            if (code == 't' || code == 'T') {
                /* Hide all text elements. */
                if (textCanvas.style.display != "none") {
                    textCanvas.style.display = "none";
                    TextRenderer.textHidden = true;
                } else {
                    textCanvas.style.display = "";
                    TextRenderer.textHidden = false;
                }
            }
        });
    }
}