'use strict'

class AnimatedSpriteType {
    constructor(initSpriteSheetTexture, initSpriteWidth, initSpriteHeight) {
        this.spriteSheetTexture = initSpriteSheetTexture;
        this.spriteWidth = initSpriteWidth;
        this.spriteHeight = initSpriteHeight;
        this.animations = new Array();

        this.rows = 0; this.cols = 0;
        this.leftBuffer = 0;
        this.rightBuffer = 0;
        this.topBuffer = 0;
        this.bottomBuffer = 0;
    }

    /* Add animations frame-by-frame to this AnimatedSpriteType. */
    addAnimation(state, framesArray) {
        var i = 0;
        for (i = 0; i < framesArray.length; i++) {
            this.addAnimationFrame(state, framesArray[i].index, framesArray[i].duration);
        }
    }

    /* Helper method for addAnimation() that pushes a single frame. */
    addAnimationFrame(state, index, frameDuration) {
        var animState = this.animations[state];
        var obj = {"index": index, "duration": frameDuration };
        animState.push(obj);
    }

    /* PROBABLY OKAY, BUT SHOULD STILL BE TESTED. */
    getAnimation(state) {
        return this.animations[state];
    }

    /* Calculate left edge bound using sprite index and number of columns in sprite sheet. */
    getLeft(state, frameIndex) {
        var stateAnim = this.animations[state];
        var ind = stateAnim[frameIndex].index;
        return (this.spriteWidth + this.leftBuffer + this.rightBuffer) * (ind % this.cols);
    }

    /* Calculate top edge bound using sprite index and number of columns in sprite sheet. */
    getTop(state, frameIndex) {
        var stateAnim = this.animations[state];
        var ind = stateAnim[frameIndex].index;
        return (this.spriteHeight + this.topBuffer + this.bottomBuffer) * Math.floor(ind / this.cols);
    }

    /* Helper method to retrieve rows and columns when loading JSON files. */
    setDims(y, x) { this.rows = y; this.cols = x; }

    /* Helper method to set buffer values. */
    setBuffers(left, right, top, bottom) {
        this.leftBuffer = left;
        this.rightBuffer = right;
        this.topBuffer = top;
        this.bottomBuffer = bottom;
    }
}