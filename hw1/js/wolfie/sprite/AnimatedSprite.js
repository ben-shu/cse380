'use strict'

class AnimatedSprite {
    constructor(initSpriteType, initState) {
        /* Save parameters to this object's data fields. */
        this.spriteType = initSpriteType;
        this.state = initState;

        /* Retrieve animations from AnimatedSpriteType object. */
        console.log(this.spriteType);
        this.anim = this.spriteType.animations[this.state];

        /* Current frame in the animations array. */
        this.animationFrameIndex = 0;
        /* Timer, incremented for each frame. */
        this.frameCounter = 0;
        /* Set length for how long each frame should stay out. */
        this.duration = this.anim[0].duration;

        /* Set up position, rotation, and scale vectors for sprite. */
        var math = window.wolfie.math;
        this.position = math.createPositionVector();
        this.rotation = math.createRotationVector();
        this.scale = math.createPositionVector();
        
        /* Initialize vectors to default position, no rotation, and normal size. */
        this.position.set(0.0, 0.0, 0.0, 1.0);
        this.rotation.set(0.0, 0.0, 0.0, 1.0);
        this.scale.set(1.0, 1.0, 1.0, 1.0);
    }

    setState(initState) {
        /* Save new current state. */
        this.state = initState;
        /* Retrieve new animation frames for new state. */
        this.anim = this.spriteType.animations[this.state];
        /* Reset frame index and total frame count. */
        this.animationFrameIndex = 0;
        this.frameCounter = this.anim[0].duration;
    }

    step() {
        /* Move on to next frame of animation. */
        if (this.frameCounter < this.duration - 1) {
            this.frameCounter++;
        } else {
            if (this.animationFrameIndex < this.anim.length - 1) {
                this.animationFrameIndex++;
                this.frameCounter = 0;
                this.duration = this.anim[this.animationFrameIndex].duration;
            } else {
				if (this.anim.repeat) {
					this.animationFrameIndex = 0;
					this.frameCounter = 0;
					this.duration = this.anim[this.animationFrameIndex].duration;
				}
			}
        }
    }

    /* Call getLeft() method from AnimatedSpriteType. */
    getLeft(state, frameIndex) {
        return this.spriteType.getLeft(state, frameIndex);
    }

    /* Call getTop() method from AnimatedSpriteType. */
    getTop(state, frameIndex) {
        return this.spriteType.getTop(state, frameIndex);
    }

    /* Method for cycling through different animation states. */
    nextAnimation() {
        if (!this.anim.repeat) {
            this.setState(this.anim.next);
        }
    }
}