'use strict'

var files = 0;

class WolfieFileManager {
    constructor() {}

    loadSpriteType(scene, jsonFilePath, callback) {
        /* Increment counter to show that we are loading a sprite type.  */
        files++;

        /* Retrieve data from JSON at specified path. */
        $.getJSON(jsonFilePath, function(json) {
            /* Retrieve Texture object using image path from JSON. */
            var texture = scene.textures[json.spriteSheetImage];

            /* Create new AnimatedSpriteType object using JSON data. */
            var spriteType = new AnimatedSpriteType(
                texture, json.spriteWidth, json.spriteHeight);

            /* Save the numbers of rows/columns in sprite's source image. */
            spriteType.setDims(json.rows, json.columns);

            /* Save the buffer values for left, right, top, bottom. */
            spriteType.setBuffers(json.leftBuffer, json.rightBuffer, json.topBuffer, json.bottomBuffer);

            /* Add all sprite animations. */
            var anims = json.animations;
            var anim, i;
            for (i = 0; i < anims.length; i++) {
                /* Retrieve animation from JSON. */
                anim = anims[i];

                /* Initialize new animation object using animation's name. */
                spriteType.animations[anim.name] = [];
                var stateAnim = spriteType.animations[anim.name];
                stateAnim.name = anim.name;
                stateAnim.repeat = anim.repeat;
                if (!stateAnim.repeat) {
                    stateAnim.next = anim.next;
                }
                /* Call addAnimation() to add the animation's frames. */
                spriteType.addAnimation(anim.name, anim.frames);
            }

            /* Add finished AnimatedSpriteType to SceneComponent. */
            scene.addAnimatedSpriteType(json.name, spriteType);

            /* Decrement file counter to show that sprite type is finished. */
            files--;

            /* If all sprite types have been loaded, run callback. */
            if (files == 0) {
                callback();
            }
        }).error(function(){
            /* Print error message to console if JSON cannot be loaded. */
            console.log("Could not load sprite types from JSON.");
            return;
        });
    }
}