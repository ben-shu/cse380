'use strict'

var WolfieStatus = {
    READY:    'READY',
    LOADED:         'LOADED',
    FAILED:         'FAILED'
};

class Wolfie {
    constructor() {}
    
    init(gameCanvasId, textCanvasId) {
        // BUILD THE COMPONENTS

        /**
         * MathComponent.js
         * Only has methods - no further init needed
         * Used in GraphicsComponent setup (GraphicsComponent.init())
        */
        this.math = new MathComponent();
        this.graphics = new GraphicsComponent();
        this.scene = new SceneComponent();
        this.ui = new UIComponent();

        // AND NOW INITIALIZE THEM IN THE PROPER ORDER
        /**
         * GraphicsComponent.js
         * Loads WebGL components, notifying browser if that fails.
         * Data Fields:
         *      renderingCanvas:    <canvas> element in HTML page
         *      canvasWidth:
         *      canvasHeight:       height and width of <canvas> element
         *      webGL:              WebGL component (provides access to render methods)
         *      spriteRenderer:     USED IN INIT() - see SpriteRenderer.js
         *                          init() uses MathComponent
         *      textRenderer:       USED IN INIT() - see TextRenderer.js
         */
        this.graphics.init(gameCanvasId, textCanvasId, this.math);

        // SETUP THE UI EVENT HANDLERS
        this.ui.init(gameCanvasId, textCanvasId, this.scene);
    }
}