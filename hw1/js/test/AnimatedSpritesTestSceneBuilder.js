'use strict'

// UPDATE THIS TO USE YOUR OWN SPRITE
var TestConstants = {
    RED_CIRCLE_MAN_SPRITE_TYPE: 'Red Circle Man',
    FORWARD_STATE: "FORWARD",
    REVERSE_STATE: "REVERSE",

    DOBBY_TYPE: 'Dobby',
    STATE_IDLE: 'IDLE',
    STATE_WALK_L: 'WALK_L',
    STATE_WALK_R: 'WALK_R',
    STATE_CAST_L: 'CAST_L',
    STATE_CAST_R: 'CAST_R',
    STATE_DYING: 'DYING',
    STATE_DEAD: 'DEAD',
    STATE_DANCING: 'DANCING'
};

class AnimatedSpritesTestSceneBuilder {
    constructor() { }

    buildScene(graphics, scene, callback) {
        var texturePaths = ["resources/images/RedCircleMan.png", "resources/images/Dobby.png"];
        var builder = this;
        graphics.loadTextures(scene, texturePaths, function () {
            builder.buildAnimatedSpriteTypes(scene, function() {
                builder.buildAnimatedSprites(scene);
                builder.buildText(graphics);
                callback();
            });
        });
    }

    buildAnimatedSpriteTypes(scene, callback) {
        var wolfieFileManager = new WolfieFileManager();
        wolfieFileManager.loadSpriteType(scene, "resources/animated_sprites/RedCircleMan.json", callback);
        wolfieFileManager.loadSpriteType(scene, "resources/animated_sprites/Dobby.json", callback);
    }

    buildAnimatedSprites(scene) {
        /* Start by adding sprites of type RED_CIRCLE_MAN. */
        var animatedSpriteType = scene.getAnimatedSpriteType(TestConstants.RED_CIRCLE_MAN_SPRITE_TYPE);
        
        /* Add alternating FORWARD and REVERSE sprites for the Red Circle Men. */
        this.sprite0 = new AnimatedSprite(animatedSpriteType, TestConstants.FORWARD_STATE);
        this.sprite0.position.set(100.0, 100.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.sprite0);

        this.sprite1 = new AnimatedSprite(animatedSpriteType, TestConstants.REVERSE_STATE);
        this.sprite1.position.set(300.0, 100.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.sprite1);
        
        this.sprite2 = new AnimatedSprite(animatedSpriteType, TestConstants.REVERSE_STATE);
        this.sprite2.position.set(500.0, 100.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.sprite2);
        
        this.sprite3 = new AnimatedSprite(animatedSpriteType, TestConstants.REVERSE_STATE);
        this.sprite3.position.set(700.0, 100.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.sprite3);
        
        this.sprite4 = new AnimatedSprite(animatedSpriteType, TestConstants.REVERSE_STATE);
        this.sprite4.position.set(900.0, 100.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.sprite4);

        /* Load DOBBY sprite type into animatedSpriteType variable. */
        animatedSpriteType = scene.getAnimatedSpriteType(TestConstants.DOBBY_TYPE);

        /* Add IDLE sprite. */
        this.spriteA = new AnimatedSprite(animatedSpriteType, TestConstants.STATE_IDLE);
        this.spriteA.position.set(100.0, 300.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.spriteA);
        /* Add DANCING sprite. */
        this.spriteB = new AnimatedSprite(animatedSpriteType, TestConstants.STATE_DANCING);
        this.spriteB.position.set(250.0, 300.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.spriteB);
        /* Add WALK_L sprite. */
        this.spriteC = new AnimatedSprite(animatedSpriteType, TestConstants.STATE_WALK_L);
        this.spriteC.position.set(400.0, 300.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.spriteC);
        /* Add WALK_R sprite. */
        this.spriteD = new AnimatedSprite(animatedSpriteType, TestConstants.STATE_WALK_R);
        this.spriteD.position.set(550.0, 300.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.spriteD);
        /* Add DYING sprite. */
        this.spriteE = new AnimatedSprite(animatedSpriteType, TestConstants.STATE_DYING);
        this.spriteE.position.set(700.0, 300.0, 0.0, 1.0);
        scene.addAnimatedSprite(this.spriteE);
    }

    buildText(graphics) {
        var text = graphics.textRenderer;
        var builder = this;
        var sprite0Text = new TextToRender("Sprite0", "", 20, 50, function () { sprite0Text.text = builder.buildSpriteSummary("Sprite0", builder.sprite0)});
        var sprite1Text = new TextToRender("Sprite1", "", 20, 80, function () { sprite1Text.text = builder.buildSpriteSummary("Sprite1", builder.sprite1)});
        var sprite2Text = new TextToRender("Sprite2", "", 20, 110, function () { sprite2Text.text = builder.buildSpriteSummary("Sprite2", builder.sprite2)});
        var sprite3Text = new TextToRender("Sprite3", "", 20, 140, function () { sprite3Text.text = builder.buildSpriteSummary("Sprite3", builder.sprite3)});
        var sprite4Text = new TextToRender("Sprite4", "", 20, 170, function () { sprite4Text.text = builder.buildSpriteSummary("Sprite4", builder.sprite4)});
        
        var spriteAText = new TextToRender("SpriteA", "", 20, 200, function() { spriteAText.text = builder.buildSpriteSummary("SpriteA", builder.spriteA)});
        var spriteBText = new TextToRender("SpriteB", "", 20, 230, function() { spriteBText.text = builder.buildSpriteSummary("SpriteB", builder.spriteB)});
        var spriteCText = new TextToRender("SpriteC", "", 20, 260, function() { spriteCText.text = builder.buildSpriteSummary("SpriteC", builder.spriteC)});
        var spriteDText = new TextToRender("SpriteD", "", 20, 290, function() { spriteDText.text = builder.buildSpriteSummary("SpriteD", builder.spriteD)});
        var spriteEText = new TextToRender("SpriteE", "", 20, 320, function() { spriteEText.text = builder.buildSpriteSummary("SpriteE", builder.spriteE)});       

        text.addTextToRender(sprite0Text);
        text.addTextToRender(sprite1Text);
        text.addTextToRender(sprite2Text);
        text.addTextToRender(sprite3Text);
        text.addTextToRender(sprite4Text);

        text.addTextToRender(spriteAText);
        text.addTextToRender(spriteBText);
        text.addTextToRender(spriteCText);
        text.addTextToRender(spriteDText);
        text.addTextToRender(spriteEText);
    }

    buildSpriteSummary(spriteName, sprite) {
        var summary = spriteName + ": { position: ("
            + sprite.position.getX() + ", " + sprite.position.getY() + ") "
            + "(state: " + sprite.state + ") "
            + "(animationFrameIndex: " + sprite.animationFrameIndex + ") "
            + "(frameCounter: " + sprite.frameCounter + ") "
            + "(duration: " + sprite.spriteType.animations[sprite.state][sprite.animationFrameIndex].duration + ")";
        return summary;
    }
}