'use strict'

class UIComponent {
    constructor() {}

    init(canvasId, scene) {
        var canvas = document.getElementById(canvasId);
        this.spriteToDrag = null;
        this.dragOffsetX = -1;
        this.dragOffsetY = -1;

        canvas.onmousedown = function(event) {
            var mousePressX = event.clientX;
            var mousePressY = event.clientY;

            console.log("X: " + mousePressX + ", Y: " + mousePressY);

            var sprite = scene.getFirstSpriteAt(mousePressX, mousePressY);
            console.log(sprite);
            if (sprite != null) {
                // START DRAGGING IT
                this.spriteToDrag = sprite;

                this.dragOffsetX = sprite.position.getX() - mousePressX;
                this.dragOffsetY = sprite.position.getY() - mousePressY;
            }
        }

        canvas.onmousemove = function(event) {
            window.wolfie.ui.currentMouseX = event.clientX;
            window.wolfie.ui.currentMouseY = event.clientY;
            if (this.spriteToDrag != null) {
                this.spriteToDrag.position.set(event.clientX + this.dragOffsetX, event.clientY + this.dragOffsetY, this.spriteToDrag.position.getZ(), this.spriteToDrag.position.getW());
                window.wolfie.graphics.renderScene(window.wolfie.scene);
            }
        }

        canvas.onmouseup = function(event) {
            this.spriteToDrag = null;
        }

        canvas.onmouseleave = function(event) {
            this.spriteToDrag = null;
        }

        document.addEventListener("keydown", function(e) {
            var code = e.key;
            if (code == " ") {
                console.log("Space pressed!");
                if (!window.wolfie.physics.jump) {
                    window.wolfie.physics.inputY = -60.0;
                    window.wolfie.physics.jump = true;
                }
            } else if (code == "a" || code == "d") {
                console.log(code + " pressed!");
                window.wolfie.physics.inputX = (code == "a") ? -16.0 : 16.0;
                window.wolfie.physics.move = true;
            }
        });

        document.addEventListener("keyup", function(e) {
            var code = e.key;
            if (code == "a" || code == "d") {
                console.log(code + " released!");
                window.wolfie.physics.move = false;
            }
        });

    }
}