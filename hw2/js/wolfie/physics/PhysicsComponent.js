'use strict'

class PhysicsComponent {
    constructor() { 
        this.gravity = 5.0;
        this.windOrCurrent = 0.0;
        this.currentTime = 0.0;

        this.playerObj = null;
        this.platform = null;

        this.inputX = 0.0;
        this.inputY = 0.0;
        this.jump = false;
        this.move = false;

        // WE'LL KEEP OUR COLLIDABLE OBJECTS SORTED BY X-COORDINATE USING THEIR SWEPT SHAPES
        this.collidableObjects = new Array();
        this.collisions = new Array();
        this.recyclableCollisions = new Array();
        for (var i = 0; i < 1000; i++) {
            var recyclableCollision = new Collision();
            this.recyclableCollisions.push(recyclableCollision);
        }
    }

    addCollidableObject(collidableObjectToAdd) {
        this.collidableObjects.push(collidableObjectToAdd);
    }

    removeCollidableObject(collidableObjectToRemove) {
        var indexOfItemToRemove = this.collidableObjects.indexOf(collidableObjectToRemove);
        this.collidableObjects.splice(indexOfItemToRemove, 1);
    }

    getCollidableObject(index) {
        return this.collidableObjects[index];
    }

    // YOU MUST DEFINE THE METHODS BELOW

    /*
     * sortCollidableObjects - this method must sort the collidable objects by their
     *                         swept shape's left edge.
     */
    sortCollidableObjects() {
        /* Sort collidableObjects array using quicksort algorithm. */
        //this.collidableObjects = this.quickSortObj(this.collidableObjects);
        
        /* Sort collidableObjects[] using mergesort algorithm. */
        this.collidableObjects = this.mergeSortObj(this.collidableObjects);
    }

    mergeSortObj(arr) {
        /* If array length is less than 1, it is sorted. */
        if (arr.length <= 1) { return arr; }
        /* Find center of array and split into left and right sublists. */
        var center = Math.floor(arr.length / 2), left = arr.slice(0, center), right = arr.slice(center);
        /* Call mergesort on left[] and right[], then merge the resulting lists. */
        return this.mergeObj(this.mergeSortObj(left), this.mergeSortObj(right));
    }

    mergeObj(arr1, arr2) {
        /* Declare empty array to store sorted items. */
        var arr = [];
        /* While both lists have items, push the smaller head item into arr[]. */
        while (arr1.length > 0 && arr2.length > 0) {
            if (arr1[0].boundingVolume.getLeft() < arr2[0].boundingVolume.getLeft()) {
                arr.push(arr1.shift());
            } else {
                arr.push(arr2.shift());
            }
        }
        /* When one list is empty, push all remaining items from other list into arr[]. */
        if (arr1.length > 0) { return arr.concat(arr1); }
        else if (arr2.length > 0) { return arr.concat(arr2); }
        else { return arr; }
    }

    /*
     * sortCollisions - this method must sort collisions by their time of collision.
     */
    sortCollisions() {
        /* Sort collisions[] using quicksort algorithm. */
        //this.collisions = this.quickSortColl(this.collisions);

        /* Sort collisions[] using mergesort algorithm. */
        this.collisions = this.mergeSortColl(this.collisions);
    }

    mergeSortColl(arr) {
        /* If array length is 1 or less, it is sorted. */
        if (arr.length <= 1) { return arr; }
        /* Declare control variables for mergesort algorithm. */
        var center = Math.floor(arr.length / 2), left = arr.slice(0, center), right = arr.slice(center);
        /* Split list into two halves, then call mergesort on each half. */
        return this.mergeColl(this.mergeSortColl(left), this.mergeSortColl(right));
    }

    mergeColl(arr1, arr2) {
        /* Declare empty array to store sorted items. */
        var arr = [];
        /* While both arrays have items, push the smaller head item into arr[]. */
        while (arr1.length > 0 && arr2.length > 0) {
            if (arr1[0].timeOfCollision < arr2[0].timeOfCollision) {
                arr.push(arr1.shift());
            } else {
                arr.push(arr2.shift());
            }
        }
        /* When one list is empty, push all remaining items from other list into arr[]. */
        if (arr1.length > 0) { return arr.concat(arr1); }
        else if (arr2.length > 0) { return arr.concat(arr2); }
        else { return arr; }
    }

    /*
     * moveAll - this method moves all the collidable objects up to the time provided.
     */
    moveAll(time) {
        /* Call every collidableObject's move() method. */
        var i = 0;
        for (i = 0; i < this.collidableObjects.length; i++) {
            this.collidableObjects[i].move(this.collidableObjects[i].currentTime, time);
        }
    }

    /*
     * calculateTimeOfCollision - this method calculates the time of collision between
     *                            the two collidable objects referenced in the collision
     *                            argument.
     */
    calculateTimeOfCollision(collision) {
        //console.log("Calculating...");
        /* Retrieve both objects from collision. */
        var obj1 = collision.collidableObject1, obj2 = collision.collidableObject2;
        
        /* Calculate time of collision in x-axis using x-velocities and distance between centers. */
        var vX = Math.abs(obj2.physicalProperties.velocityX - obj1.physicalProperties.velocityX);
        var dX = Math.abs(obj2.boundingVolume.centerX - obj1.boundingVolume.centerX)
                - (obj2.boundingVolume.width / 2) - (obj1.boundingVolume.width / 2);
        var timeX = (vX != 0.0) ? (dX / vX) : -1.0;

        /* Calculate time of collision in y-axis using y-velocities and distance between centers. */
        var vY = Math.abs(obj2.physicalProperties.velocityY - obj1.physicalProperties.velocityY);
        var dY = Math.abs(obj2.boundingVolume.centerY - obj1.boundingVolume.centerY)
                - (obj2.boundingVolume.height / 2) - (obj1.boundingVolume.height / 2);
        var timeY = (vY != 0.0) ? (dY / vY) : -1.0;

        /* If timeX or timeY are out of (0, 1], then collision did not occur this frame. */
        if ((timeX <= 0.0 && timeY <= 0.0) || (timeX > 1.0 || timeY > 1.0)) {
            /* Find invalid Collision in this.collisions[] and remove it from the array. */
            var index = this.collisions.indexOf(collision);
            if (index > -1) {
                this.recyclableCollisions.push(this.collisions.splice(index, 1));
                collision.timeOfCollision = 2.0;
                collision.startTimeOfCollisionX = 2.0;
                collision.startTimeOfCollisionY = 2.0;
                collision.endTimeOfCollisionX = 2.0;
                collision.endTimeOfCollisionY = 2.0;
            }
        /* Otherwise, update remaining data fields. */
        } else {
            //console.log("Filling out collision...");
            collision.timeOfCollision = ((timeX > timeY) ? timeX : timeY);
            collision.startTimeOfCollisionX = timeX;
            collision.startTimeOfCollisionY = timeY;
            collision.endTimeOfCollisionX = (timeX == 0) ? 0 :
            (dX + (obj1.width / 2) + (obj2.width / 2)) / vX;
            collision.endTimeOfCollisionY = (timeY == 0) ? 0 :
            (dX + (obj1.height / 2) + (obj2.height / 2));
        }
    }

    /* Check swept shapes for possible collisions. */
    checkSweptShapes(obj1, obj2) {
        var shape1 = obj1.sweptShape, shape2 = obj2.sweptShape;
        if (Math.abs(shape2.centerX - shape1.centerX) >= ((shape1.width / 2) + (shape2.width / 2)))
        { return false; }
        else {
            if (Math.abs(shape2.centerY - shape1.centerY) >= ((shape1.height / 2) + (shape2.height / 2)))
            { return false; }
            else { return true; }
        }
    }

    /* Adjust object velocities and positions based on collision information. */
    resolveCollision(collision) {
        /* Retrieve the objects that are colliding. */
        var obj1 = collision.collidableObject1, obj2 = collision.collidableObject2;

        /* Update currentTime of both objects. */
        obj1.currentTime = collision.timeOfCollision;
        obj2.currentTime = collision.timeOfCollision;

        /* Update positions based on time of collision and current velocities. */
        var vX1 = obj1.physicalProperties.velocityX,
        dX1 = (vX1 * collision.timeOfCollision);
        // obj1.boundingVolume.centerX += ((obj1.isStatic()) ? 0: dX1);
        
        var vY1 = obj1.physicalProperties.velocityY,
        dY1 = (vY1 * collision.timeOfCollision);
        // obj1.boundingVolume.centerY += ((obj1.isStatic()) ? 0 : dY1);
        
        obj1.moveTo(obj1.boundingVolume.centerX + ((obj1.isStatic()) ? 0 : dX1), obj1.boundingVolume.centerY + ((obj1.isStatic()) ? 0 : dY1));
        
        /* Update positions based on time of collision and current velocities. */
        var vX2 = obj2.physicalProperties.velocityX,
        dX2 = vX2 * collision.timeOfCollision;
        // obj2.boundingVolume.centerX += ((obj2.isStatic()) ? 0 : dX2);
        
        var vY2 = obj2.physicalProperties.velocityY,
        dY2 = vY2 * collision.timeOfCollision;
        // obj2.boundingVolume.centerY += ((obj2.isStatic()) ? 0 : dY2);
        
        obj2.moveTo(obj2.boundingVolume.centerX + ((obj2.isStatic()) ? 0 : dX2), obj2.boundingVolume.centerY + ((obj2.isStatic()) ? 0 : dY2));

        /* Calculate object velocities after the collision occurs. */
        if (obj1.isStatic()) {
            //console.log("Collided with static.");
            if (obj2 === this.playerObj) {
                this.platform = obj1;
            }
            if (!obj2.isWalking()) {
                //console.log("Setting velocities to 0.");
                obj2.physicalProperties.velocityX = 0;
                obj2.physicalProperties.velocityY = 0;
                if (obj2.boundingVolume.centerY < obj1.boundingVolume.centerY) { /*console.log("Setting walking.");*/ obj2.walking = true; }
            }
        } else if (obj2.isStatic()) {
            if (obj1 === this.playerObj) {
                this.platform = obj2;
            }
            //console.log("Collided with static.");
            if (!obj1.isWalking()) {
                obj1.physicalProperties.velocityX = 0;
                obj1.physicalProperties.velocityY = 0;
                if (obj1.boundingVolume.centerY < obj2.boundingVolume.centerY) { /*console.log("Setting walking.");*/ obj1.walking = true; }
            }
        } else {
            var vX = (vX1 + vX2) / 2, vY = (vY1 + vY2) / 2;
            if (obj1.physicalProperties.centerX < obj2.physicalProperties.centerX) {
                obj1.physicalProperties.velocityX = vX * -1.0;
                obj2.physicalProperties.velocityX = vX;
            } else {
                obj1.physicalProperties.velocityX = vX;
                obj2.physicalProperties.velocityX = vX * -1.0;
            }
            if (obj1.physicalProperties.centerY < obj2.physicalProperties.centerY) {
                obj1.physicalProperties.velocityY = vY * -1.0;
                obj1.walking = false;
                obj2.physicalProperties.velocityY = vY;
            } else {
                obj1.physicalProperties.velocityY = vY;
                obj2.physicalProperties.velocityY = vY * -1.0;
                obj2.walking = false;
            }
        }
    }

    /*
     *  step - this method steps the physics system through one time step, meaning one frame.
     *
     *  NOTE, YOU MUST COMPLETE THIS METHOD, IT IS ONLY PARTIALLY DEFINED
     */
    step() {
        // NOTE THAT THIS PHYSICS SYSTEM ASSUMES A FIXED-FRAME RATE, SO ALL VELOCITIES
        // AN ACCELERATIONS USE COORDINATE VALUES THAT ARE CLOCKED PER FRAME. FOR EXAMPLE,
        // A VELOCITY OF 5 WOULD MEAN IT IS MOVING 5 UNITS PER FRAME

        // THE BEGINNING OF THE TIME IS 0.0
        this.currentTime = 0.0;

        if (this.move) {
            this.playerObj.physicalProperties.velocityX = this.inputX;
        } else {
            if (this.playerObj.velocityX < 0.0) {
                if (this.playerObj.velocityX > -8.0) {
                    playerObj.velocityX = 0.0;
                } else { this.playerObj.velocityX += 8.0; }
            } else if (this.playerObj.velocityX > 0.0) {
                if (this.playerObj.velocityX < 8.0) {
                    playerObj.velocityX = 0.0;
                } else { this.playerObj.velocityX -= 8.0; }
            }
        }

        if (this.playerObj.isWalking() && this.platform != null) {
            if (Math.abs(this.playerObj.boundingVolume.centerX - this.platform.boundingVolume.centerX)
                > (this.playerObj.boundingVolume.width / 2) + (this.platform.boundingVolume.width / 2)) {
                this.playerObj.walking = false;
            }
        }

        if (this.playerObj.isWalking()) {
            if (this.jump) {
                this.playerObj.physicalProperties.velocityY += this.inputY;
                this.playerObj.walking = false;
                this.platform = null;
            }
        }
        if (this.playerObj.physicalProperties.velocityY == 0.0) { this.jump = false; }

        // START BY GOING THROUGH EACH OF THE CollidableObjects AND FOR EACH:
            // ADD GRAVITY AND OTHER ACCELERATION
            // UPDATE THEIR SWEPT SHAPE
        for (var i = 0; i < this.collidableObjects.length; i++) {
            // GET THE COLLIDABLE OBJECT
            var collidableObject = this.collidableObjects[i];
            var pp = collidableObject.physicalProperties;

            // APPLY GRAVITY AND WIND/CURRENT AND ALL OTHER ACCELERATION TO THE DYNAMIC SPRITES
            if (!collidableObject.isStatic()) {
                pp.velocityX += pp.accelerationX + this.windOrCurrent;

                // WE ONLY APPLY Y ACCELERATION TO GRAVITY IF THE OBJECT ISN'T WALKING ON A SURFACE,
                // NOTE THAT THIS CREATES AN ISSUE WHERE THE SPRITE WON'T FALL AFTER WALKING OFF
                // OF A PLATFORM, WHICH YOU'LL HAVE TO DEAL WITH
                if (!collidableObject.isWalking()) {
                    pp.velocityY += pp.accelerationY + this.gravity;
                }
                // else {
                //     console.log("walking objects get no gravity");
                // }
            }

            // NOW UPDATE THE SWEPT SHAPE
            collidableObject.sweep(this.currentTime);
        }
        
        // SORT ALL THE SWEPT SHAPES
        this.sortCollidableObjects();

        /* Check all swept shapes for possible collisions. */
        var i = 0, j = 0;
        for (i = 0; i < this.collidableObjects.length; i++) {
            for (j = i + 1; j < this.collidableObjects.length; j++) {
                if (this.collidableObjects[i].isStatic() &&
                    this.collidableObjects[j].isStatic()) { continue; }
                if (this.checkSweptShapes(this.collidableObjects[i], this.collidableObjects[j])) {
                    //console.log("Collision detected!");
                    var newCol = this.recyclableCollisions.shift();
                    newCol.collidableObject1 = this.collidableObjects[i];
                    newCol.collidableObject2 = this.collidableObjects[j];
                    this.calculateTimeOfCollision(newCol);
                    this.collisions.push(newCol);
                }
            }
        }

        /* Sort any detected collisions so that they can be handled in order. */
        this.sortCollisions();

        /* Resolve each collision in the order that they occur. */
        for (i = 0; i < this.collisions.length; i++) {
            this.resolveCollision(this.collisions[i]);
        }

        /* Recycle all collisions at end of frame. */
        while(this.collisions.length > 0) {
            var collision = this.collisions.shift();
            collision.timeOfCollision = 2.0;
            collision.startTimeOfCollisionX = 2.0;
            collision.startTimeOfCollisionY = 2.0;
            collision.endTimeOfCollisionX = 2.0;
            collision.endTimeOfCollisionY = 2.0;
            this.recyclableCollisions.push(collision);
        }

        // NOW MOVE EVERYTHING UP TO TIME 1.0
        if (this.currentTime < 1.0) {
            this.moveAll(1.0);
        }
    }
}